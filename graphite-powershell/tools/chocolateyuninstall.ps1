﻿[string]$Service = "Graphite-Powershell"
[string]$Path = "$env:SystemDrive\$Service"
[string]$Process = 'nssm'

function DeleteGraphitePowerShell {
    if ($null -ne (Get-Service -Name $Service -ErrorAction SilentlyContinue)) {
        CheckingStateGP
    }
    if ($null -ne (Get-Process -Name $Process -ErrorAction SilentlyContinue)) {
        CheckingStateNSSM
    }
    if (Test-Path -Path $Path) {
        Remove-Item  -Path $Path -Recurse -Force
    }
}
 
function CheckingStateGP {
    $Timer = 10
    $MaxAttempts = 5
    $count = 0
 
    if ((Get-Service -Name $Service).Status -eq 'Stopped') {
        Start-Process -FilePath "C:\Windows\System32\cmd.exe" -ArgumentList "/c sc delete $Service"
        return
    }
    else {
        Stop-Service -Name $Service -Force
        while ((Get-Service -Name $Service).Status -ne 'Stopped') {
            if (count -lt $MaxAttempts) {
                $count++
                Start-Sleep -seconds $timer                
            }
            else {
                $exception = New-Object -TypeName System.Exception -ArgumentList "Service $Service can not be stopped. Increase the value of the timer and try again." 
                throw $exception
            }                
        }
        Start-Process -FilePath "C:\Windows\System32\cmd.exe" -ArgumentList "/c sc delete $Service" -Wait      
    }        
}
 
function CheckingStateNSSM {
    $Timer = 10
    $MaxAttempts = 5
    $count = 0

    # The process nssm works
    if ($null -ne ((Get-Process -Name $Process -ErrorAction SilentlyContinue))) {
        $all_nssm = (Get-Process -Name $Process | Select-Object -ExpandProperty Path).count

        if($all_nssm -gt 1){
            foreach ($nssm_proc in $all_nssm) {
                # nssm.exe is located on path
                if (((Get-Process -Name $Process | Select-Object -ExpandProperty Path)[$nssm_proc]) -eq "$Path\nssm.exe") { 
                    $nssm_id = ((Get-Process -Name $Process | Select-Object -ExpandProperty Id)[$nssm_proc])
                    
                    while ($null -ne $nssm_id) {                         
                        if ($count -lt $MaxAttempts) {
                            $count++
                            Stop-Process -Id  $nssm_id -Force
                            Start-Sleep -seconds $timer
                        }
                        else {
                            $exception = New-Object -TypeName System.Exception -ArgumentList "Process $Process can not be stopped. Increase the value of the timer and try again." 
                            throw $exception
                        }
                    }
                }
            }
        }

        else {
            if (((Get-Process -Name $Process | Select-Object -ExpandProperty Path)) -eq "$Path\nssm.exe") { 
                $nssm_id = (Get-Process -Name $Process | Select-Object -ExpandProperty Id)

                while ($null -ne $nssm_id) {                         
                    if ($count -lt $MaxAttempts) {
                        $count++
                        Stop-Process -Id  $nssm_id -Force
                        Start-Sleep -seconds $timer
                    }
                    else {
                        $exception = New-Object -TypeName System.Exception -ArgumentList "Process $Process can not be stopped. Increase the value of the timer and try again." 
                        throw $exception
                    }
                }
            }
        }
    }

    Remove-Item  -Path $Path -Recurse -Force
}


try {
    DeleteGraphitePowerShell
}
catch {
    $_.Exception.Message
    return
}