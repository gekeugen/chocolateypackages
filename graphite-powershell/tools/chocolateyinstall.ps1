﻿$toolsDir = Split-Path $MyInvocation.MyCommand.Definition
[string]$Service = "Graphite-Powershell"
[string]$Path = "$env:SystemDrive\$Service"
[string]$Process = 'nssm'
  
function DeleteGraphitePowerShell {
    if ($null -ne (Get-Service -Name $Service -ErrorAction SilentlyContinue)) {
        CheckingStateGP
    }
    if ($null -ne (Get-Process -Name $Process -ErrorAction SilentlyContinue)) {
        CheckingStateNSSM
    }
    if (Test-Path -Path $Path) {
        Remove-Item  -Path $Path -Recurse -Force
    }
}

function CheckingStateNSSM {
    $Timer = 10
    $MaxAttempts = 5
    $count = 0

    # The process nssm works
    if ($null -ne ((Get-Process -Name $Process -ErrorAction SilentlyContinue))) {
        $all_nssm = (Get-Process -Name $Process | Select-Object -ExpandProperty Path).count

        if($all_nssm -gt 1){
            foreach ($nssm_proc in $all_nssm) {
                # nssm.exe is located on path
                if (((Get-Process -Name $Process | Select-Object -ExpandProperty Path)[$nssm_proc]) -eq "$Path\nssm.exe") { 
                    $nssm_id = ((Get-Process -Name $Process | Select-Object -ExpandProperty Id)[$nssm_proc])
                    
                    while ($null -ne $nssm_id) {                         
                        if ($count -lt $MaxAttempts) {
                            $count++
                            Stop-Process -Id  $nssm_id -Force
                            Start-Sleep -seconds $timer
                        }
                        else {
                            $exception = New-Object -TypeName System.Exception -ArgumentList "Process $Process can not be stopped. Increase the value of the timer and try again." 
                            throw $exception
                        }
                    }
                }
            }
        }

        else {
            if (((Get-Process -Name $Process | Select-Object -ExpandProperty Path)) -eq "$Path\nssm.exe") { 
                $nssm_id = (Get-Process -Name $Process | Select-Object -ExpandProperty Id)

                while ($null -ne $nssm_id) {                         
                    if ($count -lt $MaxAttempts) {
                        $count++
                        Stop-Process -Id  $nssm_id -Force
                        Start-Sleep -seconds $timer
                    }
                    else {
                        $exception = New-Object -TypeName System.Exception -ArgumentList "Process $Process can not be stopped. Increase the value of the timer and try again." 
                        throw $exception
                    }
                }
            }
        }
    }

    Remove-Item  -Path $Path -Recurse -Force
}
 
function InstallGraphitePowerShell {
    $debugmod = $false
     
    try {
        DeleteGraphitePowerShell
    }
    catch {
        $_.Exception.Message
        return
    }
 
    $ErrorActionPreference = 'Stop'
 
    if ((Get-OSArchitectureWidth 64) -and $env:chocolateyForceX86 -ne 'true') {
        Write-Host "Installing 64 bit version"; $is64 = $true
    }
    else { Write-Host "Installing 32 bit version"} 
     
    $packageArgs = @{
        PackageName  = 'nssm'
        FileFullPath = Get-Item $toolsDir\nssm*.zip
        Destination  = $toolsDir
    }
     
    Get-ChildItem $toolsDir\* | Where-Object { $_.PSISContainer } | Remove-Item -Recurse -Force #remove older package dirs
    Get-ChocolateyUnzip @packageArgs
    $source = if ($is64) { 'win64' } else { 'win32' }
     
    $packageArgs = @{
        PackageName  = $Service
        FileFullPath = Get-Item "$toolsDir\$Service*.zip"
        Destination  = $toolsDir
    }
     
    Get-ChocolateyUnzip @packageArgs
     
    Rename-Item -Path "$toolsDir\Graphite-PowerShell-Functions-master" -NewName "$toolsDir\$Service"
     
    Copy-Item $toolsDir\nssm-*\$source\nssm.exe "$toolsDir\$Service"
    Copy-Item -Path "$toolsDir\$Service" -Destination "$env:SystemDrive\" -Recurse -Force
     
    # Remove garbage
    Remove-Item -Recurse "$toolsDir\$Service"
    Remove-Item -Force $toolsDir\nssm*.zip -ea 0
    Remove-Item -Recurse $toolsDir\nssm-*
     
    # Configure nssm
    Start-Process -FilePath $Path\nssm.exe -ArgumentList "install $Service  powershell.exe -command & { Import-Module $Path\Graphite-PowerShell.psm1 ; Start-StatsToGraphite }" -Wait
    Start-Process -FilePath "C:\Windows\System32\cmd.exe" -ArgumentList "/c sc failure $Service actions= restart/60000/restart/60000/restart/60000// reset= 240"
    Start-Process -FilePath $Path\nssm.exe -ArgumentList "set  $Service  AppRotateFiles 1"
    Start-Process -FilePath $Path\nssm.exe -ArgumentList "set  $Service  AppRotateOnline 1"
    if ($debugmod) {
        Start-Process -FilePath $Path\nssm.exe -ArgumentList "set  $Service AppStderr $Path\stdout.txt"
        Start-Process -FilePath $Path\nssm.exe -ArgumentList "set  $Service AppStdout $Path\stdout.txt"
    }
    Start-Process -FilePath $Path\nssm.exe -ArgumentList "set  $Service  AppThrottle 1500"
}

InstallGraphitePowerShell